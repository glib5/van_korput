import numpy as np
import matplotlib.pyplot as plt
from os import system, name

'''this is an example of "quasi-random numbers":
they aim at uniforming filling the n-D hypercube
(see also: low discrepancies sequencies)'''


##from speed import mytimer as T


def vdk_seq(k, bas=2):
    '''returns the first k elements of the Van Der Korput sequence, any base'''
    return np.asarray([(sum((int(e)/(bas**i))
                            for i,e in enumerate(("0"+np.base_repr(n, base=bas, padding=0)[::-1]))))
                            for n in range(k)])


##def vdk_seq(k, bas=2):
##    seq = []
##    for n in range(k):
##        s = 0
##        for i, e in enumerate(("0"+np.base_repr(n, base=bas, padding=0)[::-1])):
##            s += int(e)/(bas**i)
##        seq.append(s)
##    return np.asarray(seq)


##def vdk_seq2(k, bas=2):
##    seq = np.empty(shape=k)
##    for n in range(k):
##        seq[n] = sum(int(e)/(bas**i) for i, e in enumerate(("0"+np.base_repr(n, base=bas, padding=0)[::-1])))
##    return seq



#--------------------------------

def ex1():
    tot = 7
    for i in range(1, tot):
        k = 1<<i
        sq = vdk_seq(k)
        plt.scatter(sq, np.ones_like(sq)*i, s=16, label="%d"%k)
    plt.legend()
    plt.title("Van der Korput sequence, base 2")
    plt.show()

def ex11():
    tot = 7
    for i in range(1, tot):
        k = 3**i
        sq = vdk_seq(k, 3)
        plt.scatter(sq, np.ones_like(sq)*i, s=16, label="%d"%k)
    plt.legend()
    plt.title("Van der Korput sequence, base 2")
    plt.show()

def ex2(k):
    x = vdk_seq(k)# binary
    y = vdk_seq(k, 3) # base 3
    plt.scatter(x, y, s=16)
    kk = (1<<4)*k
    x = vdk_seq(kk)# binary
    y = vdk_seq(kk, 3) # base 3
    plt.scatter(x, y, s=1)
    plt.title("Van der Korput sequence")
    plt.xlabel("Base 2")
    plt.ylabel("Base 3")
    plt.show()

def ex3(tot):
    S = np.arange(tot+1, 1, -1)*3
    for i in range(tot):
        k = 1<<i
        x = vdk_seq(k)# binary
        y = vdk_seq(k, 3) # base 3
        plt.scatter(x, y, s=S[i], alpha=S[i]/S[0], color="k")
    plt.show()


def ex4(tot):
    plt.figure()
    for i in range(tot-1):
        k = 1<<i
        x = vdk_seq(k)# binary
        y = vdk_seq(k, 3) # base 3
        
        plt.clf()
        plt.ylim(-.1, 1.1)
        plt.xlim(-.1, 1.1)
        
        plt.scatter(x, y, s=1, color="k")
        plt.pause(0.2)
        
    k = 1<<tot
    x = vdk_seq(k)# binary
    y = vdk_seq(k, 3) # base 3
    plt.clf()
    plt.ylim(-.1, 1.1)
    plt.xlim(-.1, 1.1)
    plt.scatter(x, y, s=1, color="k")
    plt.title("Increasing sequence")
    plt.show()
        
    

#--------------------------------------------

def main():
    if name=='nt':
        system('cls')
    ex1()
    ex11()
##    ex2(1<<8)
##    ex3(15)
    ex4(17)       
    return 0

if __name__=="__main__":
    main()

##    m = 200
##
##    vdk_seq(m)
##    vdk_seq2(m)

















    
